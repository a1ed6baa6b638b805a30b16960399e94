FINEID
======

Card
----

`pcsc_scan` (from [pcsc-tools](https://pcsc-tools.apdu.fr/)) output:

```
3B 7F 96 00 00 80 31 B8 65 B0 85 04 02 1B 12 00 F6 82 90 00
	Finnish ID-card v5.0(?) (eID)
	https://dvv.fi/en/fineid-specifications
```

S/MIME with Mutt, gnupg-pkcs11, GPGME & mpollux
-----------------------------------------------

The communication flow is as follows: mutt -> gpgme -> (gpgsm) -> gpg-agent -> gnupg-pkcs11-scd -> libcryptoki -> smartcard

### Prerequisites

* [pkcs11-helper](https://github.com/OpenSC/pkcs11-helper)
* [gnupg-pkcs11](http://gnupg-pkcs11.sourceforge.net/)
    * Version > 0.9.2 because of [#17](https://github.com/alonbl/gnupg-pkcs11-scd/issues/17)

### Configuration

`~/.gnupg/gpg-agent.conf`:

```
scdaemon-program /usr/bin/gnupg-pkcs11-scd
```

`~/.gnupg/gnupg-pkcs11-scd.conf`:

```
providers p1
provider-p1-library /usr/lib64/libcryptoki.so
```

`muttrc`:

```
set crypt_use_gpgme
set smime_default_key="12345678.0" <- check your ID with the commands described below in "Usage"
set nohonor_disposition
```

### Usage

```
gpgsm --learn-card
```

Run `gpgsm --list-keys` and check the `ID` of your cert with `key usage: digitalSignature keyEncipherment dataEncipherment`.

You can test whether your card is identified by running `gpg-agent --server gpg-connect-agent` and issuing the `SCD LEARN` command.

S/MIME with Mutt, OpenSSL & mpollux
-----------------------------------

### Prerequisites

* [libp11](https://github.com/OpenSC/libp11)
* [mpollux](https://dvv.fi/kortinlukijaohjelmisto)

Determine your private key ID with `pkcs11-tool --module /usr/lib64/libcryptoki.so --list-objects --type privkey`. It should look like this:

```
Private Key Object; RSA 
  label:      todentamis- ja salausavain
  ID:         45
  Usage:      decrypt, sign
```

Determine the IDs of your "todentamis- ja salausvarmenne" certificate and the intermediate CA "VRK Gov. CA for Citizen Certificates - G3" certificate with the following command:

```
pkcs11-tool --module /usr/lib64/libcryptoki.so --list-objects --type cert
```

Extract the "todentamis- ja salausvarmenne" and "VRK Gov. CA for Citizen Certificates - G3" certificates from the card into separate files with (repeat and save into different files):

```
pkcs11-tool --module /usr/lib64/libcryptoki.so --read-object --id KEY_ID --type cert --output-file cert.der
```

Convert them from DER to PEM (repeat for both certificates):

```
openssl x509 -in cert.der -inform DER -out cert.pem -outform PEM
```

### Configuration

#### OpenSSL

Create the following OpenSSL configuration (see [Using the engine from the command line](https://github.com/OpenSC/libp11#using-the-engine-from-the-command-line)):

```
openssl_conf = openssl_init

[openssl_init]
engines = engine_section

[engine_section]
pkcs11 = pkcs11_section

[pkcs11_section]
engine_id = pkcs11
dynamic_path = /usr/lib64/engines-1.1/pkcs11.so
MODULE_PATH = /usr/lib64/libcryptoki.so
init = 0
```

#### Mutt

* You need to disable GPGME in Mutt, so you can use the old school `smime_*_command`s
* `cert.pem` is the extracted "todentamis- ja salausvarmenne" certificate
* `ca.pem` is the "VRK Gov. CA for Citizen Certificates - G3" intermediate CA certificate
* The key ID is hardcoded and determined in the "Determine your private key ID" part
* `smime_default_key` and `smime_sign_as` are just dummy values so Mutt doesn't prompt you for the key ID

Add the following to your `muttrc`:

```
unset crypt_use_gpgme
set smime_default_key="12345678.0"
set nohonor_disposition
set smime_decrypt_command="OPENSSL_CONF=/path/to/openssl.conf openssl smime -decrypt -engine pkcs11 -keyform engine -inform DER -in %f -inkey 45"
set smime_sign_as="87654321.0"
set smime_sign_command="OPENSSL_CONF=/path/to/openssl.conf openssl smime -sign -engine pkcs11 -keyform engine -signer /path/to/cert.pem -certfile /path/to/ca.pem -inkey 45 -in %f -outform DER"
```

### Usage

Mutt will ask you for a passphrase, but as this comes from mpollux you can enter anything to the prompt.

SSH
---

If you have [#1608](https://github.com/OpenSC/OpenSC/pull/1608) patch, you can get your SSH public key with `pkcs15-tool --read-ssh-key ID` where `ID` is the ID of your "todentamis- ja salausvarmenne" certificate. You also need to have the following in `/etc/opensc.conf` or otherwise `pkcs15-tool` will not work:

```
app default {
	card_atr 3b:7f:96:00:00:80:31:b8:65:b0:85:04:02:1b:12:00:f6:82:90:00 {
		driver = fineid;
	}
}
```

Do note that the patch does not seem to work properly with FINEID v5, except for the `pkcs15-tool` part so leaving it to your `opensc.conf` is not recommended.

Other option to convert your certificate into SSH `authorized_key` is to run `openssl x509 -in cert.pem -noout -pubkey | ssh-keygen -i -m PKCS8 -f /dev/stdin`.

You need to have the following in your SSH client configuration (`ssh_config`):

```
Host ...
	PKCS11Provider /usr/lib64/libcryptoki.so
```

Then just add the public key into your `authorized_key` and you can authenticate with your FINEID card.

References
----------

* [Architecture Details: How the components work together](https://www.gnupg.org/documentation/manuals/gnupg/Component-interaction.html)
